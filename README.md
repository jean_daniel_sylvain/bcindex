# README #

# What is bcindex #
* This package *bcindex* is intend to illustrate the behaviour of several similarity indices for characterization and quantification of the level and direction of change in plant communities (or any other communities) relative to a given state of reference. The tool was implemented to illustrate a simulation and a case study use in the paper:  *A new index to quantify both the amplitude and the direction of spatio-temporal changes in species richness and composition in biological communities*. It also contains several functions used to map results.

# Version : 0.1
# License: GPL-2

#Summary of set up#
 1.   Get the package from bitbucket (https://bitbucket.org/jean_daniel_sylvain/bcindex/downloads)
 2.   Install the package from archives in R
 3.   In Rgui: Packages -> Install package(s) from local files.
 4.   From R terminal : install.packages('bcindex0.1.zip',repos=NULL)

#Dependencies#
* raster, sp, lattice, grDevices, grid, graphics, gtools, unmarked, rgeos, png, unmarked, raster, reshape, rgeos, VennDiagram.

#How to run the demo#
* library(bciindex)
* bcindex::demos("result-case-simulation-paper")
* bcindex::demos("result-case-study-paper")

# Further developments
* Implementation of occurence index.