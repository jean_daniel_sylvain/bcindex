# Call package
require(raster)
require(lattice)
require(graphics)
require(grDevices)
require(sp)
require(png)
require(unmarked)
require(rgeos)

# Set.seed
set.seed(1977)
oldpar <- par();

# Preparation of the dataset
# simulate environment

# Generate waterbalance data
data(Switzerland) # elevation data

# Calculate log of elevation
Switzerland$logelev <- log(Switzerland$elevation)

# normalize dataset
means <- mean(Switzerland$logelev)
vars <- var(Switzerland$logelev)
norm.elev <- (Switzerland$logelev-means)/var(Switzerland$logelev)
Switzerland$wbal <- ((norm.elev))*50;

# Giving a gradient west-to east
Switzerland$wbal2 <- (Switzerland$x/10000);
norm.wbal2 <- 10*(Switzerland$wbal2-mean(Switzerland$wbal2))
Switzerland$wbal2 <- norm.wbal2


dev.new(noRStudioGD = TRUE,width = 1000,height = 2000)
par(mfrow=c(4,1))

hist(Switzerland$elevation,
     main="Original data",
     xlab="Elevation of Switzerland - package Unmarked");


hist(Switzerland$logelev,
     main="Centralizing data",
     xlab="Log(Elevation) of Switzerland - package Unmarked");


hist(Switzerland$wbal,
     main="Simulating water balance by normalizing log(elevation)",
     xlab="Water balance (mm)", col="red") ;

hist(Switzerland$wbal2, main="E-W induced change of water balance",
     xlab="Change in water balance (mm)",  col="blue",
     xlim=range(Switzerland$wbal)) ;

cuts <- hist(Switzerland$wbal, nclass=22,plot=FALSE)

par <- oldpar

# create raster dataset
ff3 <- matrix(1/9,3,3);
ff5 <- matrix(1/25,5,5);
ff7 <- matrix(1/49,9,9)
r1 <- rasterFromXYZ(Switzerland[,c("x","y","wbal")]);
rforest <- rasterFromXYZ(Switzerland[,c("x","y","forest")]);
rwater <- rasterFromXYZ(Switzerland[,c("x","y","water")]);


r1.f <- focal(x =r1,w=ff3);#r1.f <- focal(x =r1.f,w=ff3);r1.f <- focal(x =r1.f,w=ff3);
r2 <- rasterFromXYZ(Switzerland[,c("x","y","wbal2")]);
r2 <- -1*r2
r2.f <- focal(x =r2,w=ff3);#r2.f <- focal(x =r2.f,w=ff3);r2.f <- focal(x =r2.f,w=ff5);
r3.f <- r1.f+r2.f;
r3 <- r1+r2;



# Define legend and lab
minval <- min(cellStats(r1.f,"range"),cellStats(r2.f,"range"),cellStats(r3,"range"));
maxval <- max(cellStats(r1.f,"range"),cellStats(r2.f,"range"),cellStats(r3,"range"));
brk = c(minval,cuts$mid,maxval)  # Cr?er les ?carts pour l'attribution de couleur
nbrks <- round((0.2*length(brk)+length(brk))) # manipulation to extract the end of the colorbar
jet.colors <- rainbow(nbrks);
brk.lab <- c(round(minval),cuts$mid[seq(1,length(cuts$mid),2)],round(maxval));


# Define wet status for each quadrat
ext <- extent(r1);
rangex <- ext@xmax-ext@xmin;
rangey <- ext@ymax-ext@ymin;
scale.pos.x <- ext@xmin+rangex*0.1;
scale.pos.y <- ext@ymax-rangey*0.1;


# Divide the plot infunciton of initial gradient and final gradient
midlineNS <- Line(cbind(c(ext@xmin+rangex/2,ext@xmin+rangex/2),c(ext@ymin,ext@ymax)));
midlineEW <- Line(cbind(c(ext@xmin, ext@xmax), c(ext@ymin+rangey/2,ext@ymin+rangey/2)));
S1 <- Lines(list(midlineNS),ID = "a" );
S2 <- Lines(list(midlineEW),ID = "b" );
midlines <- SpatialLines(list(S1,S2));

pointlist <- cbind(rbind(ext@xmin+rangex/4,ext@xmin+(3*rangex/4),
                         ext@xmin+(3*rangex/4),ext@xmin+rangex/4),
                   rbind(ext@ymin+(3*rangey/4),ext@ymin+(3*rangey/4),
                         ext@ymin+(rangey/4),ext@ymin+(rangey/4)));
wetstatus <- c("dry/wetter", "dry/drier",
               "wet/drier", "wet/wetter");
points <- SpatialPoints(coords=pointlist);




# Figure 1 :
dev.new(noRStudioGD = TRUE,width = 2000,height = 1500)
par(mfrow=c(2,2),oma=c(0,4,4,2))
brk.r <- intersect(which(brk>cellStats(r1,'range')[1]),which(brk<cellStats(r1,'range')[2]));

plot(r1, col=jet.colors[brk.r],zlim=cellStats(r1,'range'),cex.axis=0.8,
     axis.args=list(at=brk.lab,
                    labels=brk.lab,
                    cex.axis=0.8),
     sub=list("(a) Initial water balance",cex=1.8),
     legend.args=list(text='mm/year', side=4, font=2, line=2.5, cex=0.8), asp=1);

r2range <- cellStats(r2,"range")
brk.delta <- seq(signif(r2range[1],2),signif(r2range[2],2),by=50);
rwb.color <- colorRampPalette(c("red","white", "blue"));

plot(r2, col=rwb.color(length(brk.delta)),cex.axis=0.8,
     axis.args=list(at=brk.delta,
                    labels=brk.delta,
                    cex.axis=0.8),
     sub=list("(b) Change in water balance (mm/year)",cex=1.8),
     legend.args=list(text='mm/year', side=4, font=2, line=2.5, cex=0.8),asp=1);


brk.r <- intersect(which(brk>cellStats(r3,'range')[1]),which(brk<cellStats(r3,'range')[2]))


plot(r3,col=jet.colors[brk.r],zlim=cellStats(r3,'range'),cex.axis=0.8,
     axis.args=list(at=brk.lab,
                    labels=brk.lab,
                    cex.axis=0.8),
     sub=list("(c) Final water balance (mm/year)",cex=1.8),
     legend.args=list(text='mm/year', side=4, font=2, line=2.5, cex=0.8),asp=1);


# Plot graph
plot(r1, col="grey",
     sub=list("(d) Schema of water balance status",cex=1.8),
     legend=FALSE,cex.axis=0.8,);
plot(midlines,add=TRUE);
text(x = points, labels = wetstatus, cex=2);





# Define range of values
nsp=10
# attribute range for environmental gradient
rmin <- cellStats(r1 ,stat='min');
rmax <- cellStats(r1 ,stat='max');
rsd <- cellStats(r1 ,stat='sd');
rmin=rmin+1.5*rsd; # limit the range of environmental gradient e.g. (desert)
rmax=rmax-1.5*rsd;
rrange <- cellStats(r1 ,stat='range');
rrange <- rrange;
rsd.thres = seq(0.1,2,by=2/nsp);
thres = sample(rsd.thres,size = nsp,replace = TRUE);

# Simulate 10 species normal distribution /several temperature
sp.norm <- spnorm(npop = 1000,nspecies = nsp,
                  meanx =seq((rmin),(rmax),
                             by = (((rmax-(0.05*rsd))-((rmin)+(0.05*rsd)))/(nsp-1))),
                  sdx=thres*rsd);

# Simulate 10 species with gamma distribution
sp.beta <- spbeta(npop = 1000,nspecies = nsp,
                  type =sample(x = c('cold','hot','generalist','random'),nsp,
                               prob =c(0.5,0.2,0.1,0.2) , replace=TRUE),
                  vrange = rrange);


# Define probabilities for each species
sp.domain <- cbind( sp.norm,sp.beta$mat)#sp.beta$spmat);
size=dim(sp.domain)[2];
qt <- apply(sp.domain,2,quantile, c(0.2,0.8));


# Classify raster

# Create species layer classifying quantile on time one (xcl2) and time two (xcl2)
xcl <- sp_layer(r1.f,qt,x1min = -Inf,x1max = Inf) ;
names(xcl) <-  c("elev",                rep(paste('sp',1:size)));
xcl <- dropLayer(xcl,1);

xcl2 <- sp_layer(r3.f,qt,x1min = -Inf,x1max = Inf) ;
names(xcl2) <- c("elev",                rep(paste('sp',1:size)));
xcl2 <- dropLayer(xcl2,1);



dev.new(width = 1500,height = 1000,noRStudioGD = TRUE)

sp1 <- spplot(xcl, colorkey=FALSE, col.regions=c("dark blue","white"),
              strip=FALSE,
              cex.main=0.5,
              main= "Initial species distribution",
              par.settings = list(strip.background=list(col="white"),
                                  strip.border=list(col="white"),
                                  strip.shingle=list(col="white"),
                                  axis.line=list(col="white"),
                                  add=TRUE
              ));
print(sp1)




dev.new(width = 1500,height = 1000,noRStudioGD = TRUE)
sp2 <-  spplot(xcl2, colorkey=FALSE, col.regions=c("dark red","white"),
               strip=FALSE,
               cex.main=0.5,
               main= "Final species distribution",
               par.settings = list(strip.background=list(col="white"),
                                   strip.border=list(col="white"),
                                   strip.shingle=list(col="white"),
                                   axis.line=list(col="white"),
                                   add=TRUE
               ));
print(sp2)




# Create binary maps of distribution of each species
sel <- subset(xcl, 1:size)
sel2 <- subset(xcl2, 1:size)

a <- sel&sel2;
a[Which(rwater==100)] <- 0;
b <- sel&!sel2;
b[Which(rwater==100)] <- 0;
c <- sel2&!sel;
c[Which(rwater==100)] <- 0;

suma <- sum(a);
sumb <- sum(b);
sumc <- sum(c);
sumn <- suma+sumb+sumc;


par(mfrow=c(2,2))
minval <- cellStats(sumn, min);
maxval <- cellStats(sumn, max);
brk.sr <- seq(0,maxval,1) # Creer les ecarts pour l'attribution de couleur
btwcolor <- colorRampPalette(c("red","orange","wheat","light blue", "darkblue"))(length(brk.sr))

# Plot value of CSR, RSAB, RSBA, TSR

# prepare contour of the data
if(!exists(x = "polyraster")){polyraster <- rasterToPolygons(r1/r1, dissolve=TRUE)};

# init list of overlays
spl <- list('sp.polygons', polyraster, cex=2, col='black'); # polygons


pcsr <- spplot(suma,breaks=brk.sr , col.regions=btwcolor,at=brk.sr,
               colorkey=FALSE, asp=1,
               sp.layout=list(spl),
               sub=list(label="a) Shared suitable habitats before and after changes \n in water balance conditions",
                        cex=1.5, just=0.5)
)


psrab <-  spplot(sumb,breaks=brk.sr , col.regions=btwcolor,at=brk.sr,
                 colorkey=FALSE, asp=1,
                 sp.layout=list(spl),
                 sub=list(label="b) Suitable habitats unique to initial water \n balance conditions",
                          cex=1.5, just=0.5)
)

psrba <-  spplot(sumc,breaks=brk.sr , col.regions=btwcolor,at=brk.sr,
                 colorkey=FALSE, asp=1, asp=1,
                 sp.layout=list(spl),
                 sub=list(label="c) Suitable habitats unique to final water \n balance conditions",
                          cex=1.5, jus=0.5)
)

ptsr <-  spplot(sumn,breaks=brk.sr , col.regions=btwcolor,at=brk.sr,
                colorkey=FALSE, asp=1,
                sp.layout=list(spl),
                sub=list(label="d) Total of suitable habitats before and after \n changes in water balance conditions",
                         cex=1.5, jus=0.5)
)
pleg <- rastleg(vmin = min(brk.sr),vmax = max(brk.sr),lag = -1,rev(btwcolor),
                user_cex=1, dev_h = 20, dev_w=5)


# order matters
m <- rbind(c(0.0, 0.5,0.45,  1),
           c(0.45, 0.5,0.9,  1),
           c(0.0,  0, 0.45,0.5),
           c(0.45, 0, 0.9, 0.5),
           c(0.92,0.05,1,1))


dev.new(noRStudioGD = TRUE,width = 4000,height = 2500, res=1000)
par(mfrow=c(3,2),mar = c(6,2,2,2), oma=c(0,0,0,0),mai=c(5,0,0,0));


print(pcsr,position= m[1,], more=TRUE)
print(psrab,position=m[2,], more=TRUE)
print(psrba,position=m[3,], more=TRUE)
print(ptsr,position=m[4,], more=TRUE)
print(pleg,position=m[5,])
#dev.off()


# Raster Indices
bci <- bci_coef(suma,sumb,sumc,res(suma)[1]);
bnes <- bnes_coef(suma[],sumb[],sumc[]);
rbnes <- bci;
rbnes <-setValues(rbnes, 0)
rbnes <- setValues(rbnes,values = bnes);
bnes <- rbnes;
rbnes <- NULL;

pmi <- pmi_coef(suma,sumb,sumc);
ddc <- -1*disp_coef(suma,sumb,sumc);
ddc <- ddc/cellStats(ddc, 'max')
jaccard <- suma/(suma+sumb+sumc);
tsi <- tsi_coef(suma,sumb,sumc)

# Calculate tbci
#tbci <- tbci_coef(suma[],sumb[],sumc[], 100)
#rtbci <- bci;
#rtbci <- setValues(rtbci, 0)
#rtbci <- setValues(rtbci,values = tbci)
#tbci <- rtbci;

# Convert similarity indices to dissimilarity indices
sel1 <- Which(bci>0, cells=TRUE, na.rm=TRUE);
sel2 <- Which(bci<=0, cells=TRUE, na.rm=TRUE);
Dbci <- bci;
Dbci[sel1] <- 1-Dbci[sel1];
Dbci[sel2] <- -1-Dbci[sel2];
Dpmi <- 1-pmi;
Djaccard <- 1-jaccard;
Dtsi <- 1- tsi;

# bci_prob
obj <- bci_prob(suma,sumb,sumc,100)
obj <- stack(obj[[1]],obj[[2]],obj[[3]],obj[[4]],suma, sumb, sumc, sumn, (sumb-sumc))

names(obj) <- c("pSCISRI","pSCIcSRI","pSCISRIc","pSCIcSRIc","CSR","SRAB","SRBA","TSR","USR")
corRast <- layerStats(obj, 'pearson',na.rm = TRUE)
round(corRast[[1]],3)




# Decomposing change in four components
pSCISRI <- obj[[1]]
pSCIcSRI <- obj[[2]]
pSCISRIc <- obj[[3]]
pSCIcSRIc <- obj[[4]]

# Plot index
stack.bci <- stack(pSCISRI,pSCIcSRI)
stack.bci <- stack(stack.bci,pSCISRIc)
stack.bci <- stack(stack.bci,pSCIcSRIc)
names(stack.bci) <- expression("pSCISRI","pSCIcSRI", "pSCISRIc","pSCIcSRIc")
#round(cor(cbind(pAB[],pAcB[],pABc[],pAcBc[]),use = "complete.obs"),2)



# Figure case 8 species

# Prepare maphisto
dens_djac <- maphist(r = Djaccard, axis.name = FALSE,histrange = c(0,1));
dens_dbci <- maphist(Dbci, axis.name = FALSE,histrange = c(-1,1));
dens_dtsi <- maphist(Dtsi, axis.name = FALSE,histrange = c(0,1));
dens_dpmi <- maphist(Dpmi, axis.name = FALSE,histrange = c(0,1));
dens_ddc <- maphist(ddc, axis.name = FALSE,histrange = c(-1,1));
dens_bnes <- maphist(bnes, axis.name = FALSE,histrange = c(0,1));
#dens_tbci <- maphist(tbci, axis.name = FALSE,histrange = c(-1,1));



dens_pSCISRI <- maphist(r = stack.bci$pSCISRI, axis.name = FALSE,histrange = c(-1,1));
dens_pSCIcSRI <- maphist(r = stack.bci$pSCIcSRI, axis.name = FALSE,histrange = c(-1,1));
dens_pSCISRIc <- maphist(r = stack.bci$pSCISRIc, axis.name = FALSE,histrange = c(-1,1));
dens_pSCIcSRIc <- maphist(r = stack.bci$pSCIcSRIc, axis.name = FALSE,histrange = c(-1,1));



# setup color scheme
col.leg <- def_legend(c(0,1))

# generate list of trellis settings
tps <- list(regions=list(superpose.polygon=list(col='black'),
                         superpose.symbol=list(col='black')));

# init list of overlays
spl <- list('sp.polygons', polyraster, cex=2, col='black'); # polygons

# setup trellis options
trellis.par.set(tps);

# initial plot, missing key
col.leg <- def_legend(c(0,1))

# hist & plot 1
sphist <- sp_addimg(dens_djac,xcent = 5.28e+05, ycent=2.70e+05,hu=90000,wu=90000)
p1 <- spplot(Djaccard, col.regions=col.leg[[1]],at=col.leg[[2]],colorkey=FALSE,
             sp.layout=list(spl,sphist), sub="a) Dissimilarity Jaccard index")
# hist & plot 2
sphist<- sp_addimg(dens_dtsi,xcent = 5.28e+05, ycent=2.70e+05,hu=90000,wu=90000)
p2 <- spplot(Dtsi, col.regions=col.leg[[1]],at=col.leg[[2]],colorkey=FALSE,
             sp.layout=list(spl,sphist), sub="b) Dissimilarity tripartite similarity index")
# hist & plot 3
sphist<- sp_addimg(dens_dpmi,xcent = 5.28e+05, ycent=2.70e+05,hu=90000,wu=90000)
p3 <- spplot(Dpmi,col.regions=col.leg[[1]],at=col.leg[[2]],colorkey=FALSE,
             sp.layout=list(spl,sphist), sub="c) Dissimilarity positive matching index")
# hist & plot 4
sphist<- sp_addimg(dens_bnes,xcent = 5.28e+05, ycent=2.70e+05,hu=90000,wu=90000)
p4 <- spplot(bnes, col.regions=col.leg[[1]],at=col.leg[[2]],colorkey=FALSE,
             sp.layout=list(spl,sphist), sub="d) Beta nestedness index")
# hist & plot 5
col.leg <- def_legend(c(-1,1))
sphist<- sp_addimg(dens_ddc,xcent = 5.28e+05, ycent=2.70e+05,hu=90000,wu=90000)
p5 <- spplot(ddc, col.regions=col.leg[[1]],at=col.leg[[2]],colorkey=FALSE,
             sp.layout=list(spl,sphist), sub="e) Coefficient of dispersal direction")
# hist & plot 6
sphist<- sp_addimg(dens_dbci,xcent = 5.28e+05, ycent=2.70e+05,hu=90000,wu=90000)
p6 <- spplot(Dbci, col.regions=col.leg[[1]],at=col.leg[[2]],colorkey=FALSE,
             sp.layout=list(spl,sphist), sub="f) Dissimilarity biochange index")
p7 <- rastleg(-1,1,-0.1,col.leg[[1]], user_cex=1, dev_h = 20, dev_w=5)


# order matters
m <- rbind(c(0.0, 0.7,0.45,  1),
           c(0.45, 0.7,0.9,  1),
           c(0.0,  0.35, 0.45,0.7),
           c(0.45, 0.35, 0.9, 0.7),
           c(0.0,  0.0,0.45, 0.35),
           c(0.45, 0, 0.9,  0.35),
           c(0.86,0.05,1,1))

# save to file: note that we have to reset the 'regions' colors
# png(file='spplot_examples.png', width = 7000,height = 7000,res =720)
dev.new(noRStudioGD = TRUE,width = 3500,height = 4000, res=1000)
par(mfrow=c(3,2),mar = c(6,2,2,2), oma=c(0,0,0,0),mai=c(5,0,0,0));


print(p1,position= m[1,], more=TRUE)
print(p2,position=m[2,], more=TRUE)
print(p3,position=m[3,], more=TRUE)
print(p4,position=m[4,], more=TRUE)
print(p5,position=m[5,], more=TRUE)
print(p6,position=m[6,], more=TRUE)
print(p7,position=m[7,])
#dev.off()


# Plot tbci
# order matters

# initial plot, missing key
col.leg <- def_legend(c(-1,1))

# hist & plot 1
sphist <- sp_addimg(dens_pSCISRI,xcent = 5.28e+05, ycent=2.70e+05,hu=90000,wu=90000)
subtitle <-   expression(paste("(a) ",italic(P),"(",SRI,intersect(SCI),")"))
p1 <- spplot(stack.bci$pSCISRI, col.regions=col.leg[[1]],at=col.leg[[2]],colorkey=FALSE,
             sp.layout=list(spl,sphist), sub=subtitle)


# hist & plot 2
sphist<- sp_addimg(dens_pSCIcSRI,xcent = 5.28e+05, ycent=2.70e+05,hu=90000,wu=90000)
subtitle <-   expression(paste("(b) ",italic(P),"(","SRI"^"c",intersect(SCI),")"))

p2 <- spplot(stack.bci$pSCIcSRI, col.regions=col.leg[[1]],at=col.leg[[2]],colorkey=FALSE,
             sp.layout=list(spl,sphist), sub=subtitle)
# hist & plot 3
sphist<- sp_addimg(dens_pSCISRIc,xcent = 5.28e+05, ycent=2.70e+05,hu=90000,wu=90000)
subtitle <-   expression(paste("(c) ",italic(P),"(",SRI,intersect(SCI)^"c",")"))

p3 <- spplot(stack.bci$pSCISRIc,col.regions=col.leg[[1]],at=col.leg[[2]],colorkey=FALSE,
             sp.layout=list(spl,sphist), sub=subtitle)
# hist & plot 4
sphist<- sp_addimg(dens_pSCIcSRIc,xcent = 5.28e+05, ycent=2.70e+05,hu=90000,wu=90000)
subtitle <-   expression(paste("(d) ",italic(P),"(","SRI"^"c",intersect(SCI)^"c",")"))
p4 <- spplot(stack.bci$pSCIcSRIc, col.regions=col.leg[[1]],at=col.leg[[2]],colorkey=FALSE,
             sp.layout=list(spl,sphist), sub=subtitle)

p5 <- rastleg(-1,1,-0.1,col.leg[[1]], user_cex=1, dev_h = 20, dev_w=5)


# order matters
m <- rbind(c(0.0, 0.5,0.45,  1),
           c(0.45, 0.5,0.9,  1),
           c(0.0,  0.0, 0.45,0.5),
           c(0.45, 0.0, 0.9, 0.5),
           c(0.9,0.05,1,1))


dev.new(noRStudioGD = TRUE,width = 6000,height = 4000, res=1000)

par(mfrow=c(2,2),mar = c(6,2,2,2), oma=c(0,0,0,0),mai=c(5,0,0,0));

print(p1,position= m[1,], more=TRUE)
print(p2,position=m[2,], more=TRUE)
print(p3,position=m[3,], more=TRUE)
print(p4,position=m[4,], more=TRUE)
print(p5,position=m[5,])

dev.off()







dev.new(noRStudioGD = TRUE,width = 3500,height = 4000, res=1000)

par(mfrow=c(1,1),mar = c(6,2,2,2), oma=c(0,0,0,0),mai=c(5,0,0,0));

################################################################################
#' Table 3: Pearson product-moment correlations (r) for relationships between
#'          dissimilarity indices that were calculated in the simulated case study,
#'          water balance conditions, and each component of species richness.
#'          Interpretation of the correlation coefficients for CDD and DBCI differ
#'          from those of the other indices due to the difference in their range of
#'          possible values.
#'
################################################################################
#
sumb.c <- sumb-sumc
indices <- stack(r1,r2,r3,Djaccard, Dtsi, Dpmi, bnes,ddc,Dbci, suma, sumb, sumc, sumb+sumc, sumb-sumc)
names(indices) <-  c("wbalini","deltawbal","wbalfin","Djaccard","Dtsi","Dpmi",
                      "Bnes","CDD","Dbci","CSR","SRAB","SRBA","TSR","DUSR")
# #round(rast.cor.abs[[1]][4:9,c(1:3,9:14)],2)

# # Absolute correlation betweeen Indices, components of species richness and water balance
rast.cor <- layerStats(indices, stat='pearson', asSample=FALSE, na.rm=TRUE)

cat('Table 3 Pearson product-moment correlations (r) for relationships between
            dissimilarity indices that were calculated in the simulated case study,
    water balance conditions, and each component of species richness.
    Interpretation of the correlation coefficients for CDD and DBCI differ
    from those of the other indices due to the difference in their range of
    possible values.')
round(rast.cor[[1]][4:9,c(1:3,10:14)],2)

#cat('Table 4.1 Compare values of absolute indices. BCI is very similar to Jaccard index')
#rast.cor.abs <- layerStats(abs(indices), stat='pearson', asSample=FALSE, na.rm=TRUE)
#round(rast.cor.abs[[1]][4:9,c(1:3,9:14)],2)



# Absolute correlation betweeen Indices, components of species richness and water balance
indices <- stack( obj[[4]],obj[[1]], obj[[2]], obj[[3]],  r1,r2,r3,  suma, sumb, sumc,  sumb+sumc, sumb-sumc)
indices
names(indices) <-  c("pSCIcSRIc","pSCISRI","pSCIcSRI","pSCISRIc",
                     "wbalini","deltawbal","wbalfin",
                     "CSR","SRAB","SRBA","USR","DUSR")
rast.cor <- layerStats(abs(indices), stat='pearson', asSample=FALSE, na.rm=TRUE)


cat('Table 4 Pearson product-moment correlations (r) of the relationships between
             absolute values of the probabilities of change that were calculated in the
             simulated case study, water balance conditions and each component of species
             richness. ')
round(rast.cor[[1]][1:4,c(5:12)],2)


## Verify which correlation is statistically significant from zero
ind1 <- indices[]
ind2 <- indices[]
nvar = dim(ind1)[2]

pval.mat <- as.data.frame(matrix(0,nvar,nvar))
estimate.mat <- as.data.frame(matrix(0,nvar,nvar))
stats.mat <- as.data.frame(matrix(0,nvar,nvar))

for(i in 1:nvar){
  for (j in 1: nvar){
    test <- cor.test(ind1[,i],ind1[,j])
    pval.mat[i,j] <- test$p.value
    estimate.mat[i,j] <- test$estimate
    stats.mat[i,j] <- test$statistic
  }
}

names(pval.mat) <- names(indices)
names(estimate.mat) <- names(indices)
names(stats.mat) <- names(indices)

round(pval.mat,2)>0
round(estimate.mat,2)
round(stats.mat,1)

